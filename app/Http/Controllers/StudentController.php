<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Models\Student;
use App\Helpers\Helper;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $student= Student::all();
        return view('admin.users.form')->with('student',$student);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $help = Helper::studentcreate($request);
        $request->validate($help);
       
        $input=$request->all();
        if($request->image){
            $imageData = Student::image($request);
            $input['image'] = $imageData;
        }
        $newStudent = Student::create($input);
        $request->session()->flash('msg','Inserted Sucessfully!!');
        return redirect('allusers');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //fetching data
       $student=Student::where('id',$id)->first();
       if (is_null($student)) {
         return "Record not Found";
       }
       return view ('admin.users.form')->with(compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        {
            $update = Helper::studentcreate($request,$id);
            $request->validate($update);
            $student = Student::find($id);
        
            $input=$request->all();
            if($request->image){
                $imageData = Student::updateimage($request,$id);
                $input['image'] = $imageData;
            }
            $student->update($input);
                $request->session()->flash('msg','update Sucessfully!!');
                 return redirect('allusers');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Student::destroy($id);

        return response()->json(['status'=>'Student Deleted Successfully']);
    }
}
