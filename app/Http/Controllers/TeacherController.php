<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;
use App\Models\Teacher;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $teacher= Teacher::all();
        return view('admin.teachers.all_teachers')->with('teacher',$teacher);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('admin.teachers.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $help = Helper::teachercreate($request);
        $request->validate($help);
       
        $input=$request->all();
        if($request->image){
            $imageData = Teacher::image($request);
            $input['image'] = $imageData;
        }
        $new = Teacher::create($input);
        $request->session()->flash('msg','Inserted Sucessfully!!');
        return redirect()->route('teacher.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //fetching data
       $teacher=Teacher::where('id',$id)->first();
      
       return view ('admin.teachers.form')->with(compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Helper::teachercreate($request,$id);
        $request->validate($update);
        $teacher = Teacher::find($id);
    
        $input=$request->all();
        if($request->image){
            $imageData = Teacher::updateimage($request,$id);
            $input['image'] = $imageData;
              }
        $teacher->update($input);
    
            $request->session()->flash('msg','update Sucessfully!!');
             return redirect()->route('teacher.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Teacher::destroy($id);

        return response()->json(['status'=>'Teacher Deleted Successfully']);
    }
}
