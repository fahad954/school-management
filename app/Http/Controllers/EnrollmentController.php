<?php

namespace App\Http\Controllers;
use App\Models\Student;
use Illuminate\Support\Facades\Validator;
use App\Models\Course;
use App\Models\Teacher;
use App\Models\Enrollment;
use App\Helpers\Helper;
use Illuminate\Http\Request;

class EnrollmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $enrollment =  Enrollment::with('student','teacher','course')->get();
        return view('admin.enrollments.all_enrollment')->with('enrollment',$enrollment);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
          
        $student = Student::all();
        $course =  Course::all();
        $teacher = Teacher::all();
        $enrollment =  Enrollment::with('student','teacher','course')->get();
        return view('admin.enrollments.form', compact('student','course','teacher','enrollment'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $help = Helper::createenrollment($request);
        $request->validate($help);
        $input=$request->all();
        $student = Enrollment::create($input);
        $request->session()->flash('msg','Inserted Sucessfully!!');
        return redirect()->route('enrollment.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //fetching data

        $student = Student::all();
        $course =  Course::all();
        $teacher = Teacher::all();
       $enrollment=Enrollment::where('id',$id)->first();
       if (is_null($enrollment)) {
         return "Record not Found";
       }
       return view ('admin.enrollments.form')->with(compact('student','course','teacher','enrollment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Helper::createenrollment($request,$id);
        $request->validate($update);
        $enrollment = Enrollment::find($id);
    
        $input=$request->all();
        $enrollment->update($input);
        $request->session()->flash('msg','update Sucessfully!!');
             return redirect()->route('enrollment.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Enrollment::destroy($id);

        return response()->json(['status'=>'Enrollment Deleted Successfully']);
    }
}
