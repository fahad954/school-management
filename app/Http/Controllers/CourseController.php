<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;
use App\Models\Course;
class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $course= Course::all();
        return view('admin.courses.all_courses')->with('course',$course);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('admin.courses.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
         
        $help = Helper::coursecreate($request);
        $request->validate($help);
        $input = $request->all();
        $course = Course::create($input);
        $request->session()->flash('msg','Inserted Sucessfully!!');
        return redirect()->route('course.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //fetching data
       $course=Course::where('id',$id)->first();
       if (is_null($course)) {
         return "Record not Found";
       }
       return view ('admin.courses.form')->with(compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Helper::coursecreate($id,$request);
        $request->validate($update);
        $course = Course::find($id);
        $input = $request->all();
            $course->update($input);
    
            $request->session()->flash('msg','update Sucessfully!!');
             return redirect()->route('course.index');
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Course::destroy($id);

        return response()->json(['status'=>'Course Deleted Successfully']);
    }
}
