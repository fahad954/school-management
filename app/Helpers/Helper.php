<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class Helper{
  public static function studentcreate($request,$id=null){

        return $validatedData =[
             'image' => 'sometimes|image|mimes:jpeg,png,jpg|max:2048',
             'registration' => 'max:15|sometimes',
             'name' => 'required|max:40|sometimes',
             'number' => ['max:15','sometimes'],
             'email' => 'sometimes|required|email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|unique:students,email,'.$id,
             'dob' => 'sometimes',
             'address' => 'max:50|sometimes',
             'country' => 'max:10|sometimes',
             'state' => 'max:10|sometimes',
             'city' => 'max:10|sometimes'
         ];
     }
     public static function coursecreate($request,$id=null){
       return $validatedData =[
            'course_code' => 'required|sometimes|max:10',
            'name' => 'required|sometimes|max:20',
        ]; 
     }
     public static function teachercreate($request, $id=null){
       return $validatedData =[
            'image' => 'sometimes|image|mimes:jpeg,png,jpg|max:2048',
            'designation' => 'required|sometimes|max:20',
            'name' => 'required|sometimes|max:40',
            'number' => 'max:13|sometimes',
            'email' => 'sometimes|required|email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|unique:teacher,email,'.$id,
            'dob' => 'sometimes',
            'address' => 'max:70|sometimes',
            'country' => 'max:20|sometimes',
            'state' => 'max:20|sometimes',
            'city' => 'max:20|sometimes'
        ];
     }

     public static function createenrollment($request, $id=null){
       return $validatedData =[
            'student' => 'required|sometimes',
            'course' => 'required|sometimes',
            'teacher' => 'required|sometimes',
        ];
     }
}

?>