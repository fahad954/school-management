<?php

namespace App\Models;
use App\Models\Teacher;
use File;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;
    protected $table = 'teacher';
    protected $primaryKey = 'id';
    protected $fillable = ['image','name','designation','mobile','email','dob','address','country','state','city'];

    public static function image($request){
        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension();
        $filename= time().'.'.$extension;
        $file->move('images/',$filename);
        $image =$filename;

       return $image;
    }
    public static function updateimage($request,$id){
        $teacher = Teacher::find($id);
        if ($teacher) {
            // Upload the new image
            if ($request->hasFile('image')) {
                // Delete old image
               File::delete('public/images/'.$teacher->image);
    
                // Image edit
                $image =  $request->file('image');
                $extension = $image->getClientOriginalExtension();
                $filename= time().'.'.$extension;
                $image->move('images/',$filename);
    
                $image = $filename;
                return $image;
            }
          }
      }
}
