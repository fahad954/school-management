<?php

namespace App\Models;
use Illuminate\Http\Request;
use App\Models\Student;
use File;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $table = 'students';
    protected $primaryKey = 'id';
    protected $fillable = ['image','registration_no','name','mobile','email','dob','address','country','state','city','is_active'];

  public static function image($request){
    $file = $request->file('image');
    $extension = $file->getClientOriginalExtension();
    $filename= time().'.'.$extension;
    $file->move('images/',$filename);
    $image =$filename;

    return $image;
  }  
  public static function updateimage($request,$id){
    $student = Student::find($id);
    if ($student) {
        // Upload the new image
        if ($request->hasFile('image')) {
            // Delete old image
           File::delete('public/images/'.$student->image);

            // Image edit
            $image =  $request->file('image');
            $extension = $image->getClientOriginalExtension();
            $filename= time().'.'.$extension;
            $image->move('images/',$filename);

            $image = $filename;
            return $image;
        }
      }
  }
}
