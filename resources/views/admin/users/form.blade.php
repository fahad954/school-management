@extends("layouts.header")
@if(!isset($student->id))
@section('title', 'Add Student')
@else
@section('title', 'Edit Student')
@endif
@section('content')
<style>
  .form-input {
  width:250px;
  padding:20px;
  background:#fff;

}
.form-input input {
  display:none;
}
.form-input label {
  display:block;
  width:70%;
  height:50px;
  line-height:50px;
  text-align:center;
  background:#333;
  color:#fff;
  font-size:13px;
  font-family:"Open Sans",sans-serif;
  text-transform:Uppercase;
  font-weight:600;
  border-radius:10px;
  cursor:pointer;
}
 
.form-input img {
  width:100%;
  margin-top:10px;
}
#file-ip-1{
  background-image: url({{url('dist/img/default.jpg')}}) 0 0;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        @if(!isset($student->id))
          <div class="col-sm-6">
            <h1>Add Student</h1>
          </div>
          @else
          <div class="col-sm-6">
            <h1>Edit Student</h1>
          </div>
          @endif
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Basic Information</h3>
              @if(Session::has('msg'))
					<div class="col-md-12">
						<div class="alert alert-success">{{Session::get('msg')}}</div>
					</div>
					@endif
        
            </div>
            <div class="card-body">
        
             @if(!isset($student->id))
            <form method = "post" action="{{route('student.store')}}" enctype="multipart/form-data" >
            @else
             <form method = "post" action="{{route('student.update',$student->id)}}" enctype="multipart/form-data" >
             {{ method_field('PUT') }}
            @endif
             @csrf
            <div class="form-group">
            <div class="form-input">
            <div class="preview">
              @if(isset($student->image))
            <img width="50%" src="{{ URL::asset('images/'.$student->image) }}"> 
             @else
      <img id="file-ip-1-preview" src="{{URL::asset('dist/img/default.jpg')}}">
      @endif
    </div>          
    <label for="file-ip-1">Upload Image</label>
    <input type="file" id="file-ip-1" accept="image/*"  name="image" onchange="showPreview(event);">
    
    @if($errors->any())

							         <p class="text-danger">{{$errors->first('image')}}</p> 

											@endif
  </div>
              </div>
              <!-- start row -->
              <div class="row">
               <div class="col-md-4">
               <div class="form-group">
              <label for="inputName">Registration No</label>
              <input type="text" id="registration" name="registration_no"  value="{{ isset($student->registration_no) ? $student->registration_no : '' }}" class="form-control">
              @if($errors->any())

							                 <p class="text-danger">{{$errors->first('registration')}}</p>

											@endif
               </div>
               </div>
               <div class="col-md-4">
               <div class="form-group">
                <label for="inputDescription">Name</label><span class="text-danger">*</span>
                <input type="text" id="inputName" name="name" value="{{ isset($student->name) ? $student->name : '' }}" required class="form-control">
                @if($errors->any())

							                 <p class="text-danger">{{$errors->first('name')}}</p>

											@endif
              </div>
               </div>
               <div class="col-md-4">
               <div class="form-group">
                <label for="inputClientCompany">Mobile No</label>
                <input type="text" id="txtPhone" name="mobile" value="{{ isset($student->mobile) ? $student->mobile : '' }}" class="form-control">
                <span id="spnPhoneStatus"></span>
                @if($errors->any())

							                 <p class="text-danger">{{$errors->first('number')}}</p>

											@endif
              </div>
               </div>
              </div>
            
               <!-- end row -->

               <!-- start row -->
               <div class="row">
               <div class="col-md-4">
               <div class="form-group">
                <label for="inputProjectLeader">Email</label><span class="text-danger">*</span>
                <input type="email" name="email" value="{{ isset($student->email) ? $student->email : '' }}" class="form-control" required>
                @if($errors->any())

							                 <p class="text-danger">{{$errors->first('email')}}</p>

											@endif
              </div>
               </div>
               <div class="col-md-4">
               <div class="form-group">
                <label for="inputProjectLeader">DOB</label>
                <input type="text" name="dob" id="datepicker" value="{{ isset($student->dob) ? $student->dob : '' }}"  class="form-control">
                @if($errors->any())

							                 <p class="text-danger">{{$errors->first('dob')}}</p>

											@endif
              </div>
               </div>
               <div class="col-md-4">
               <div class="form-group">
                <label for="inputProjectLeader">Address</label>
                <input type="text" name="address" value="{{ isset($student->address) ? $student->address : '' }}" class="form-control">
                @if($errors->any())

							                 <p class="text-danger">{{$errors->first('address')}}</p>

											@endif
              </div>
               </div>
               </div>
             <!-- end row -->
              
                <!-- start row -->
                <div class="row">
               <div class="col-md-4">
               <div class="form-group">
                <label for="inputProjectLeader">Country</label>
                <input type="text" name="country" value="{{ isset($student->country) ? $student->country : '' }}" class="form-control">
                @if($errors->any())

							                 <p class="text-danger">{{$errors->first('country')}}</p>

											@endif
              </div>
               </div>
               <div class="col-md-4">
               <div class="form-group">
                <label for="inputProjectLeader">State</label>
                <input type="text" name="state" value="{{ isset($student->state) ? $student->state : '' }}" class="form-control">
                @if($errors->any())

							                 <p class="text-danger">{{$errors->first('state')}}</p>

											@endif
              </div>
               </div>
               <div class="col-md-4">
               <div class="form-group">
                <label for="inputProjectLeader">City</label>
                <input type="text" name="city" value="{{ isset($student->city) ? $student->city : '' }}" class="form-control">
                @if($errors->any())

							                 <p class="text-danger">{{$errors->first('city')}}</p>

											@endif
              </div>
               </div>
               </div>
             <!-- end row -->
             @if(isset($student->is_active))
             <!-- start-row -->
             <div class="row">
             <div class="col-md-4">
               <div class="form-group">
                  <label>Status</label>
                  <select class="form-control select2"  name="is_active"  required autofocus>
                  <option value="nonselected" selected disabled>---Select Status---</option>
									<option value="1" {{ $student->is_active == 1 ? 'selected' : '' }}>Active</option>
                  <option value="0" {{ $student->is_active == 0 ? 'selected' : '' }}>Hidden</option>
									</select>
                </div>
               </div>
               </div> 
               <!-- end-row -->
             @else
             <!-- start-row -->
             <div class="row">
             <div class="col-md-4">
               <div class="form-group">
                  <label>Status</label>
                  <select class="form-control select2"  name="is_active"  required autofocus>
                  <option value="nonselected" selected disabled>---Select Status---</option>
									<option value="1">Active</option>
                  <option value="0">Hidden</option>
									</select>
                </div>
               </div>
               </div> 
               @endif
               <!-- end-row -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
         <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


@endsection
@section('scripts')
<script type="text/javascript">

function showPreview(event){
  if(event.target.files.length > 0){
    var src = URL.createObjectURL(event.target.files[0]);
    var preview = document.getElementById("file-ip-1-preview");
    preview.src = src;
    preview.style.display = "block";
  }
}

</script>
<script>
$(document).ready(function () {
        var today = new Date();
        $('#datepicker').datepicker({
            format: 'mm-dd-yyyy',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });


        $('#datepicker').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });
</script>

<script>
function validatePhone(txtPhone) {
    var a = document.getElementById(txtPhone).value;
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(a)) {
        return true;
    }
    else {
        return false;
    }
}
</script>

<script>  
    $(document).ready(function() {
      $('.error').hide();
      $('#submit').click(function(){
        var email = $('#email').val();
        if(email== ''){
          $('#email').next().show();
          return false;
        }
        if(IsEmail(email)==false){
          $('#invalid_email').show();
          return false;
        }
    return false;
  });
 });
 function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(email)) {
    return false;
  }else{
    return true;
  }
}
</script>
@endsection