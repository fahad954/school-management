@extends("layouts.header")
@if(!isset($enrollment->id))
@section('title', 'Add Enrollment')
@else
@section('title', 'Edit Enrollment')
@endif
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        @if(!isset($enrollment->id))
        <div class="col-sm-6">
        <h1>Add Enrollment</h1>
          </div>
        @else
          <div class="col-sm-6">
          <h1>Edit Enrollment</h1>
          </div>
          @endif
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Basic Information</h3>
              @if(Session::has('msg'))
					<div class="col-md-12">
						<div class="alert alert-success">{{Session::get('msg')}}</div>
					</div>
					@endif
        
            </div>
            <div class="card-body">
            @if(!isset($enrollment->id))
            <form method = "post" action="{{route('enrollment.store')}}" enctype="multipart/form-data" >
            @else
             <form method = "post" action="{{route('enrollment.update',$enrollment->id)}}" enctype="multipart/form-data" >
             {{ method_field('PUT') }}
            @endif
          
             @csrf
              </div>
              <!-- start row -->
          <div class="row">
          @if(isset($enrollment->student_id))
          <div class="col-md-6">
               <div class="form-group">
                  <label>Select Student</label><span class="text-danger">*</span>
                  <select class="form-control select2"  name="student_id"  required autofocus>
                  <option value="nonselected" selected disabled>---Select Student---</option>
																				@foreach($student  as $row)
																				<option value="{{$row->id}}"  {{ $row->id == $enrollment->student_id ? 'selected' : '' }}>{{$row->name}}</option>
																				@endforeach
																				</select>
                </div>
               </div>
               @else
               <div class="col-md-6">
               <div class="form-group">
                  <label>Select Student</label><span class="text-danger">*</span>
                  <select class="form-control select2"  name="student_id"  required autofocus>
                  <option value="nonselected" selected disabled>---Select Student---</option>
																				@foreach($student  as $row)
																				<option value="{{$row->id}}" 'selected' : ''>{{$row->name}}</option>
																				@endforeach
																				</select>
                                        @if($errors->any())

                                                 <p class="text-danger">{{$errors->first('student')}}</p>

                                        @endif
                </div>
               </div>
               @endif
                <!-- End-Student-Col -->
                @if(isset($enrollment->course_id))
                <div class="col-md-6">
               <div class="form-group">
                  <label>Select Course</label><span class="text-danger">*</span>
                  <select class="form-control select2" name="course_id"  required autofocus>
                  <option value="nonselected" selected disabled>---Select Course---</option>
																				@foreach($course  as $row)
																				<option value="{{$row->id}}"  {{ $row->id == $enrollment->course_id ? 'selected' : '' }}>{{$row->c_name}}</option>
																				@endforeach
																				</select>
                </div>
               </div><!-- End-course-Col -->
                @else
               <div class="col-md-6">
               <div class="form-group">
                  <label>Select Course</label><span class="text-danger">*</span>
                  <select class="form-control select2" name="course_id"  required autofocus>
                  <option value="nonselected" selected disabled>---Select Course---</option>
																				@foreach($course  as $row)
																				<option value="{{$row->id}}" 'selected' : ''>{{$row->c_name}}</option>
																				@endforeach
																				</select>
                                        @if($errors->any())

                                                 <p class="text-danger">{{$errors->first('course')}}</p>

                                        @endif
                </div>
               </div><!-- End-course-Col -->
               @endif
                </div><!-- end row -->
               
                <div class="row">
                @if(isset($enrollment->teacher_id))
                <div class="col-md-6">
                  <div class="form-group">
                  <label>Select Teacher</label><span class="text-danger">*</span>
                  <select class="form-control select2"  name="teacher_id"  required autofocus>
                  <option value="nonselected" selected disabled>---Select Teacher---</option>
																				@foreach($teacher  as $row)
																				<option value="{{$row->id}}"  {{ $row->id == $enrollment->teacher_id ? 'selected' : '' }}>{{$row->name}}</option>
																				@endforeach
																				</select>
                                        @if($errors->any())

                                                 <p class="text-danger">{{$errors->first('teacher')}}</p>

                                        @endif
                </div>
               </div><!-- End-Teacher-Col -->
                @else
                  <div class="col-md-6">
                  <div class="form-group">
                  <label>Select Teacher</label><span class="text-danger">*</span>
                  <select class="form-control select2"  name="teacher_id"  required autofocus>
                  <option value="nonselected" selected disabled>---Select Teacher---</option>
																				@foreach($teacher  as $row)
																				<option value="{{$row->id}}" 'selected' : ''>{{$row->name}}</option>
																				@endforeach
																				</select>
                                        @if($errors->any())

                                                 <p class="text-danger">{{$errors->first('teacher')}}</p>

                                        @endif
                </div>
               </div><!-- End-Teacher-Col -->
               @endif
              </div> <!-- end row -->
            </div>
            <!-- /.card-body -->
            <div class="row">
        <div class="col-12">
         <button type="submit" class="btn btn-primary" id="register" value="Register">Submit</button>
        </div>
      </div>
      </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
     
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@endsection
