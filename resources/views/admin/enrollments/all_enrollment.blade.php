@extends("layouts.header")
@section('title', 'All Enrollments')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Enrollments</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    @if(Session::has('fahad'))
				<div class="col-md-12">
					<div class="alert alert-success">{{Session::get('fahad')}}</div>
				</div>
				@endif
				@if(Session::has('msg'))
				<div class="col-md-12">
					<div class="alert alert-success">{{Session::get('msg')}}</div>
				</div>
				@endif
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- table data -->
        <!-- /.row (main row) -->
        <div class="card">
              <div class="card-header text-end">
                <a href="{{route('enrollment.create')}}"  class="btn btn-primary">Add <span>+</span></a>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>Student</th>
                    <th>Teacher</th>
                    <th>Course</th>
                    <th>Operations</th>
                  </tr>
                  </thead>
                  <tbody>
											@if($enrollment)
											@foreach($enrollment as $row)
											<tr class="odd">
                        <input type="hidden" class="delete_val" value="{{$row->id}}">
												<td>{{$row->id}}</td>
												<td>{{$row->student->name}}</td>
                        <td>{{$row->teacher->name}}</td>
                        <td>{{$row->course->c_name}}</td>
                        <td class="text-center"><a href="{{route('enrollment.edit',$row->id)}}" data-toggle="tooltip"
														title="Edit" class="btn btn-primary">Edit</a>
                            <a href="javascript:void(0)"
														class="dltbtn btn btn-danger" title="Delete" data-toggle="tooltip">
														Delete</i></a>	
                          </td>
                        
											</tr>
											@endforeach
											@else
											<tr>
											</td colspan="6">Users not added yet.<td>
											 </tr>

											 @endif

										</tbody>
									</table>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection    
@section('scripts')
<script>
  window.setTimeout(function() {
  $(".alert").fadeTo(500, 0).slideUp(500, function(){
    $(this).remove();
  });
  }, 2000);


</script>  
<script >
  $(document).ready(function(){
   
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

    $('.dltbtn').click(function (e){
      e.preventDefault();
     var delete_id = $(this).closest("tr").find('.delete_val').val();
      swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this data!",
              icon: "warning",
              buttons: true,
              dangerMode: true,
      })
      .then((willDelete) => {
                if (willDelete) {
                  
                  var data ={
                    "_token": $('input[name="csrf-token"]').val(), 
                    "id": delete_id,
                  }; 

                  $.ajax({
                    type:"DELETE",
                    url: 'enrollment/'+delete_id,
                    data: data,
                    success: function (response){
                      swal(response.status, {
                    icon: "success",
                  })
                  .then((result) => {
                    location.reload();
                  });

                    }

                  });


                }
         });


    });
  });
</script>
@endsection

