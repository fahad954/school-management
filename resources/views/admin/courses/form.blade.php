@extends("layouts.header")
@if(!isset($course->id))
@section('title', 'Add Course')
@else
@section('title', 'Edit Course')
@endif
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        @if(!isset($course->id))
          <div class="col-sm-6">
            <h1>Add Course</h1>
          </div>
          @else
          <div class="col-sm-6">
            <h1>Edit Course</h1>
          </div>
          @endif
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Basic Information</h3>
              @if(Session::has('msg'))
					<div class="col-md-12">
						<div class="alert alert-success">{{Session::get('msg')}}</div>
					</div>
					@endif
        
            </div>
            <div class="card-body">
           
             @if(!isset($course->id))
            <form method = "post" action="{{route('course.store')}}" method="POST" enctype="multipart/form-data" >
            @else
             <form method = "post" action="{{route('course.update',$course->id)}}" enctype="multipart/form-data" >
             {{ method_field('PUT') }}
            @endif 
             @csrf
              </div>
              <!-- start row -->
              <div class="row">
               <div class="col-md-6">
               <div class="form-group">
              <label for="inputName">Course Code</label><span class="text-danger">*</span>
              <input type="text" id="Name" name="c_code" required  value="{{ isset($course->c_code) ? $course->c_code : '' }}" class="form-control num" autofocus>
              @if($errors->any())

							                 <p class="text-danger">{{$errors->first('c_code')}}</p>

											@endif
               </div>
               </div>
               <div class="col-md-6">
               <div class="form-group">
                <label for="inputDescription">Course Name</label><span class="text-danger">*</span>
                <input type="text" id="inputName" name="c_name" id ="name" required value="{{ isset($course->c_name) ? $course->c_name : '' }}" class="form-control" autofocus>
                @if($errors->any())

							                 <p class="text-danger">{{$errors->first('name')}}</p>

											@endif
              </div>
               </div>
              </div>
            
               <!-- end row -->

               <div class="row">
        <div class="col-12">
         <button type="submit" class="btn btn-primary" id="register" value="Register" disabled="disabled">Submit</button>
        </div>
        </form>
      </div>
             
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
         <button type="submit" class="btn btn-primary" id="register" value="Register" disabled="disabled">Submit</button>
        </div>
        </form>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@endsection
@section('scripts')

<script type="text/javascript">
//restrict special Characters for textbox

$('input[name="c_code"]').on('keypress',function (event){
var regex = new RegExp("^[a-zA-z0-9- ]+$");
var key = String.fromCharCode(!event.charcode ? event.which : event.charcode);
if(!regex.test(key)){
  event.preventDefault();
  return false;
}
});
$('input[name="name"]').on('keypress',function (event){
var regex = new RegExp("^[a-zA-z ]+$");
var key = String.fromCharCode(!event.charcode ? event.which : event.charcode);
if(!regex.test(key)){
  event.preventDefault();
  return false;
}
});
</script>
@endsection