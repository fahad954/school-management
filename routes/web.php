<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\EnrollmentController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.login');
});

Auth::routes();

Route::get('/allusers', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
Route::resource('student', StudentController::class);
//courses Route
Route::resource('course', CourseController::class);
//Teacher Route
Route::resource('teacher', TeacherController::class);
//Enrollment Route
Route::resource('enrollment', EnrollmentController::class);